import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import Mobil from '../../components/Mobil'
import { WARNA_HITAM } from '../../utils/constant'

const DaftarMobil = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsHorizontalScrollIndicator={false}>
        <Text style={styles.title}>Daftar Mobil</Text>
        <Mobil />
        <Mobil />
        <Mobil />
        <Mobil />
        <Mobil />
        <Mobil />
        <Mobil />
      </ScrollView>
    </View>
  )
}

export default DaftarMobil

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FFFFFF'
  },  
  title: {
    marginHorizontal: 16,
    marginTop: 30,
    marginBottom: 15,
    fontWeight: '700',
    color: WARNA_HITAM,
    fontSize: 16
  }
})