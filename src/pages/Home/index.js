import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import React from 'react';
import {Mercedes, PhotoProfile} from '../../assets';
import {WARNA_HITAM, WARNA_KEDUA} from '../../utils/constant';
import Banner from '../../components/Banner';
import {ButtonIcon} from '../../components';
import Mobil from '../../components/Mobil';

const Home = () => {
  return (
    <View style={styles.page}>
      {/* ScrollView digunakan agar screen dapat di scroll
        showsHorizontalScrollIndicator={false} agar screen tidak dapat di scroll secara horizontal
      */}
      <ScrollView showsHorizontalScrollIndicator={false}>
        {/* Membuat Header pada halaman Home */}
        <View style={styles.header}>
          <Text style={styles.textName}>Hi, Bayu</Text>
          <View style={styles.profile}>
            <Text style={styles.textLocation}>Bandung</Text>
            <Image source={PhotoProfile} style={styles.photo} />
          </View>
        </View>
        <Banner />
        <View style={styles.services}>
          <ButtonIcon title="Sewa Mobil" />
          <ButtonIcon title="Oleh-Oleh" />
          <ButtonIcon title="Penginapan" />
          <ButtonIcon title="Wisata" />
        </View>
        <View style={styles.mobilPilihan}>
          <Text style={styles.label}>Daftar Mobil Pilihan</Text>
        </View>
        <Mobil />
        <Mobil />
        <Mobil />
        <Mobil />
      </ScrollView>
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.25,
    backgroundColor: WARNA_KEDUA,
    paddingHorizontal: 16,
  },
  textName: {
    fontSize: 14,
    fontWeight: '300',
    color: WARNA_HITAM,
    paddingTop: 30,
  },
  profile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textLocation: {
    fontSize: 19,
    fontWeight: '700',
    color: WARNA_HITAM,
    paddingTop: 4,
  },
  photo: {
    width: 35,
    height: 35,
    alignItems: 'baseline',
    marginTop: -7,
  },
  services: {
    // marginHorizontal: 4,
    flexDirection: 'row',
    marginTop: 28,
    justifyContent: 'space-around',
  },
  mobilPilihan: {
    paddingHorizontal: 16,
    flex: 1,
  },
  label: {
    paddingTop: 24,
    fontSize: 15,
    color: WARNA_HITAM,
    fontWeight: 'bold',
  },
});
