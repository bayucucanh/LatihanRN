import { StyleSheet, Text, View, Dimensions } from 'react-native'
import React from 'react'
import { IconTruck, IconKey, IconCamera, IconBox } from '../../assets'



const ButtonIcon = ({title}) => {
    const Icon = () => {
      // if(title === 'Sewa Mobil') {
      //   return <IconTruck />
      // } else if (title === 'Oleh-oleh') {
      //   return <IconBox />
      // } else if (title === 'Penginapan') {
      //   return <IconKey />
      // } else if (title === 'Mobil Sewa') {
      //   return <IconCamera />
      // }
      
      if (title === 'Sewa Mobil') return <IconTruck />;
      if (title === 'Oleh-Oleh') return <IconBox />;
      if (title === 'Penginapan') return <IconKey />;
      if (title === 'Wisata') return <IconCamera />;
  
      return <IconKey />;
    };

  return (
    <View style={styles.container}>
        <View style={styles.button}>
          <Icon />
        </View>
        <Text style={styles.text}>{title}</Text>
    </View>
  )
}

export default ButtonIcon

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  button: {
    width: windowWidth * 0.16,
    height: windowHeight * 0.085,
    backgroundColor: '#DEF1DF',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginBottom: 8
  },
  text: {
    fontSize: 13,
    textAlign: 'center',
    color: '#000000'
  }
})