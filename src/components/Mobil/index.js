import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {Xenia} from '../../assets';
import Feather from 'react-native-vector-icons/Feather';
import {WARNA_HITAM} from '../../utils/constant';

const Mobil = () => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={Xenia} style={styles.image} />
      <View style={styles.infoMobil}>
        <Text style={styles.merk}>Daihatsu Xenia</Text>
        <View style={styles.tersewa}>
          <View style={{flexDirection: 'row', marginRight: 17}}>
            <Feather name="users" />
            <Text style={styles.textJumlah}>4</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Feather name="briefcase" />
            <Text style={styles.textJumlah}>2</Text>
          </View>
        </View>
        <Text style={styles.price}>Rp 230.000</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Mobil;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginTop: 16,
    borderRadius: 2,
    height: 98,
    backgroundColor: '#ffffff',
    shadowColor: WARNA_HITAM,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    flexDirection: 'row',
  },
  infoMobil: {
    paddingTop: 16,
  },
  image: {
    width: 50,
    height: 34,
    marginLeft: 16,
    marginRight: 22,
    marginTop: 24,
  },
  merk: {
    fontSize: 14,
    color: WARNA_HITAM,
    fontWeight: '500',
  },
  tersewa: {
    flexDirection: 'row',
    paddingTop: 6,
  },
  textJumlah: {
    fontSize: 10,
    paddingLeft: 5,
    paddingTop: 0,
  },
  price: {
    fontSize: 14,
    fontWeight: '500',
    color: '#5CB85F',
    paddingTop: 9,
  },
});
